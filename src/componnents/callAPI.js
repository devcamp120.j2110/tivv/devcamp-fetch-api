import { Component } from "react";

class CallAPI extends Component {
    fetchApi = async (paramUrl, paramOption = {}) => {
        const response = await fetch(paramUrl,paramOption);
        const responseData = await response.json();
        return responseData;
    }
    APIGet = () => {
        this.fetchApi('https://jsonplaceholder.typicode.com/posts/1')
        .then((data) => {
            console.log(data)
        })
        .catch(console.error());

    }
    APIGetAll = () => {
        this.fetchApi('https://jsonplaceholder.typicode.com/posts')
        .then((data) => {
            console.log(data)
        })
        .catch(console.error());

    }
    APICreate = () => {
        this.fetchApi('https://jsonplaceholder.typicode.com/posts',{
            method: 'POST',
            body: JSON.stringify({
              title: 'foo',
              body: 'bar',
              userId: 1,
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
          })
        .then((data) => {
            console.log(data)
        })
        .catch(console.error());

    }
    APIUpdate = () => {
        this.fetchApi('https://jsonplaceholder.typicode.com/posts/1',{
            method: 'PUT',
            body: JSON.stringify({
              id: 1,
              title: 'foo',
              body: 'bar',
              userId: 1,
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
          })
        .then((data) => {
            console.log(data)
        })
        .catch(console.error());

    }
    APIDelete = () => {
        this.fetchApi('https://jsonplaceholder.typicode.com/posts/1', {
            method: 'DELETE',
          })
        .then(() => {
            console.log("Delete successfully!")
        })
        .catch(console.error());

    }
    render() {
        return (
            <div className="container pt-4">
                <div className="row ml-4">
                    <div className="col-2">
                        <button className="btn btn-primary" onClick={this.APIGet}>API Get</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-info" onClick={this.APIGetAll}>API Get All</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-success" onClick={this.APICreate}>API Create</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-warning" onClick={this.APIUpdate}>API Update</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-secondary" onClick={this.APIDelete}>API Delete</button>
                    </div>

                </div>
            </div>
        )
    }
}
export default CallAPI;